# Thumbor

This repository contains the configuration and deployment scripts for running Thumbor in all deployment environments.

## Sources

The Thumbor Docker image is pulled from [DockerHub](https://hub.docker.com/r/apsl/thumbor/).

## AWS Resources

All AWS resources needed by Thumbor are managed by the CloudFormation template [aws/stb-thumbor.cf.yaml](aws/stb-thumbor.cf.yml).

The stack should be called `stb-stk-{ENV}-thumbor` where {ENV} is the logical environment name, e.g. `stb-stk-dev-thumbor`.

*Please Note*: The CloudFormation template creates an IAM user for Thumbor. (The IAM username is printed in the CloudFormation Outputs.) 
Credentials must be created manually and added to the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` in the ECS task definition files:

```
[...]
{
  "name": "AWS_ACCESS_KEY_ID",
  "value": "A*******************"
},
{
  "name": "AWS_SECRET_ACCESS_KEY",
  "value": "g*****************************"
},
[...]
```

## Dependencies

Thumbor relies on the S3 images bucket and IAM policies that are managed in [the AWS Ops repository](https://bitbucket.org/sothebys/aws-ops/src/master/cloudformation/stb-images.cf.yaml).
