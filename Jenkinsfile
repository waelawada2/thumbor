#!groovy

@Library('sothebys-jenkins-shared-library@1.x') _

def appName = 'thumbor'
def deploymentEnvironment = env.DEPLOYMENT_ENVIRONMENT ?: 'dev'
def ecsCluster = "stb-${deploymentEnvironment}"
def imageName = 'apsl/thumbor'
def branchName = (binding.variables["BRANCH_NAME"] ?: env.BRANCH_NAME) ?: 'master'

def dockerRegistry = "apsl/thumbor"
def tagName = '6.3.0'

def awsCredentailsId = deploymentEnvironment == 'dev' ? 'aws-dev-credentials' : 'aws-prd-credentials'

def containerPort = 8000
def deployedEnvironments = []

def recipientsBuildNotifications = 'gregor@zurowski.net'

try {

  node {

    currentBuild.result = "SUCCESS"

    stage('Preparation') {
      echo "==============================="
      echo "Docker image: ${imageName}"
      echo "Branch: ${branchName}"
      echo "==============================="
      sh 'docker --version'
    }

    stage('Checkout') {
      git url: "https://bitbucket.org/sothebys/${appName}.git", branch: branchName, credentialsId: 'bitbucket-sothebysbuilder'
    }

    withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: awsCredentailsId, accessKeyVariable: 'AWS_ACCESS_KEY_ID', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
      stage("Deploy to ${deploymentEnvironment.toUpperCase()}") {
        deployToECS {
          application = appName
          registry = dockerRegistry
          cluster = ecsCluster
          environment = deploymentEnvironment
          tag = tagName
          port = containerPort
        }
        deployedEnvironments << "${deploymentEnvironment.toUpperCase()}"
      }
    }

    stage('Post Build') {
      sendBuildNotification {
        success = true
        recipients = recipientsBuildNotifications
        branch = branchName
      }

      // print summary about the build
      echo "==============================="
      echo "Docker image: ${dockerRegistry}:${tagName}"
      echo "Branch: ${branchName}"
      echo "==============================="
    }

  }

} catch (e) {
  currentBuild.result = 'FAILURE'
  sendBuildNotification {
    success = false
    recipients = recipientsBuildNotifications
    branch = branchName
  }
  throw e;
}
